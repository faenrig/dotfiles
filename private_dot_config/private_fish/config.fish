set fish_greeting

fundle plugin hauleth/agnoster

fundle init

source ~/.config/aliasrc
source ~/.config/setuprc

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`
[ -f /home/faenrig/packages/gb-studio/node_modules/tabtab/.completions/electron-forge.fish ]; and . /home/faenrig/packages/gb-studio/node_modules/tabtab/.completions/electron-forge.fish

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin $PATH /home/faenrig/.ghcup/bin # ghcup-env

# opam configuration
source /home/faenrig/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
