--                                   ___________
--                                  |###########\
--                                  |############\
--                                  |#####|##  ###|
--                                  |#####|##   ##|
--                                  |#####|##  ##/ 
--                         ______   |#####|##### 
--                         \#####\_/#####W|###|   
--                          \###########W |###|
--                           '#########'  |###|
--                              -----   O ----- O
--
--       Personal vim configuration of Jean Patrick Kocherscheidt <j.p.kocherscheidt@web.de>
--
--       Inspired by 'github.com/theprimeagen'

require("faenrig.packer")
require("faenrig.remap")
require("faenrig.set")
