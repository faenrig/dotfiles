" run current python file to new buffer
autocmd FileType python map <buffer> <F5> :w<CR>:exec '!/usr/bin/env python3' shellescape(@%, 1) <CR>
autocmd FileType python imap <buffer> <F5> <esc>:w<CR>:exec '!/usr/bin/env python3' shellescape(@%, 1) <CR>
