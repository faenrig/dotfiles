" run current c# file to new buffer
autocmd FileType cs map <silent> <buffer> <F5> :w<CR>:exec 'terminal /usr/bin/env dotnet run' shellescape(@%, 1) <CR>
autocmd FileType cs imap <silent> <buffer> <F5> <esc>:w<CR>:exec 'terminal /usr/bin/env dotnet run' shellescape(@%, 1) <CR>
