" run current python file to new buffer
autocmd FileType rs map <buffer> <F5> :w<CR>:exec '! cargo run' shellescape(@%, 1) <CR>
autocmd FileType rs imap <buffer> <F5> <esc>:w<CR>:exec '! cargo run' shellescape(@%, 1) <CR>
autocmd FileType rs map <buffer> <F6> :w<CR>:exec '! cargo build' shellescape(@%, 1) <CR>
autocmd FileType rs imap <buffer> <F6> <esc>:w<CR>:exec '! cargo build' shellescape(@%, 1) <CR>
autocmd FileType rs map <buffer> <F7> :w<CR>:exec '! cargo check' shellescape(@%, 1) <CR>
autocmd FileType rs imap <buffer> <F7> <esc>:w<CR>:exec '! cargo check' shellescape(@%, 1) <CR>
