"                                   ___________
"                                  |###########\
"                                  |############\
"                                  |#####|##  ###|
"                                  |#####|##   ##|
"                                  |#####|##  ##/ 
"                         ______   |#####|##### 
"                         \#####\_/#####W|###|   
"                          \###########W |###|
"                           '#########'  |###|
"                              -----   O ----- O
"
"       Personal vim configuration of Jean Patrick Kocherscheidt <j.p.kocherscheidt@web.de>
"
"       Highly inspired by 'github.com/jessarcher/dotfiles/blob/master/nvim/init.vim'
"
"------------------------------------------------------------------------------------------
" General settings
"------------------------------------------------------------------------------------------

set expandtab
set shiftwidth=4
set tabstop=4
set hidden
set signcolumn=yes:2
set relativenumber
set number
set termguicolors
set undofile
set spell
set title
set ignorecase
set smartcase
set wildmode=longest:full,full
set nowrap
set list
set listchars=tab:▸\ ,trail:·
set mouse=a
set scrolloff=8
set sidescrolloff=8
set nojoinspaces
set splitright
set clipboard=unnamedplus
set confirm
set exrc
set backup
set backupdir=~/.local/share/nvim/backup//
set updatetime=300   " Reduce time for highlighting other references
set redrawtime=10000 " Allow more time for loading syntax on large files

"------------------------------------------------------------------------------------------
" Key maps
"------------------------------------------------------------------------------------------

let mapleader = "\<space>"

nmap <leader>ve :edit ~/.config/nvim/init.vim<cr>
nmap <leader>vc :edit ~/.config/nvim/coc-settings.json<cr> " If I wanted to switch to coc
nmap <leader>vr :source ~/.config/nvim/init.vim<cr>

nmap <leader>k :nohlsearch<CR>
nmap <leader>Q :bufdo dbdelete<cr>

" Allow gf to open non-existent files
map gf :edit <cfile><cr>

" Reselect visual selection after indenting
vnoremap < <gv
vnoremap > >gv

" Maintain the cursor position when yanking a visual selection
" https://ddrscott.github.io/blog/2016/yank-withouk-jank/
vnoremap y myy`y
vnoremap Y myY`y

" When text is wrapped, move by terminal rows, not lines, unless a count is provided
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')

" Paste replace visual selection without copying it
vnoremap <leader>p "_dP

" Make Y behave like the other capitals
nnoremap Y y$

" Keep it centered
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" Open the current file in the default program
nmap <leader>x :!xdg-open %<cr><cr>

" Quickly escape to normal mode
imap jj <esc>

" Easy insertion of a trailing ; or , from insert mode
imap ;; <Esc>A;<Esc>
imap ,, <Esc>A,<Esc>

cmap w!! %!sudo tee > /dev/null %

" Easy exit out of terminal mode
tnoremap <Esc> <C-\><C-n>

"------------------------------------------------------------------------------------------
" Plugins
"------------------------------------------------------------------------------------------

" Automatically install vim-plug
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(data_dir . '/plugins')
source ~/.config/nvim/plugins/airline.vim
source ~/.config/nvim/plugins/autopairs.vim
source ~/.config/nvim/plugins/bufferathlete.vim
source ~/.config/nvim/plugins/closetag.vim
source ~/.config/nvim/plugins/cmp.vim
source ~/.config/nvim/plugins/code-dark.vim
source ~/.config/nvim/plugins/commentary.vim
source ~/.config/nvim/plugins/dispatch.vim
source ~/.config/nvim/plugins/editorconfig.vim
source ~/.config/nvim/plugins/eunuch.vim
source ~/.config/nvim/plugins/exchange.vim
source ~/.config/nvim/plugins/firenvim.vim
source ~/.config/nvim/plugins/floaterm.vim
source ~/.config/nvim/plugins/fugitive.vim
source ~/.config/nvim/plugins/fzf.vim
source ~/.config/nvim/plugins/heritage.vim
source ~/.config/nvim/plugins/lastplace.vim
source ~/.config/nvim/plugins/lion.vim
source ~/.config/nvim/plugins/lspconfig.vim
source ~/.config/nvim/plugins/markdown-preview.vim
source ~/.config/nvim/plugins/matchtagalways.vim
source ~/.config/nvim/plugins/nerdtree.vim
source ~/.config/nvim/plugins/pasta.vim
source ~/.config/nvim/plugins/peekaboo.vim
source ~/.config/nvim/plugins/polyglot.vim
source ~/.config/nvim/plugins/projectionist.vim
source ~/.config/nvim/plugins/quickscope.vim
source ~/.config/nvim/plugins/razor.vim
source ~/.config/nvim/plugins/repeat.vim
source ~/.config/nvim/plugins/rooter.vim
source ~/.config/nvim/plugins/rust.vim
source ~/.config/nvim/plugins/sayonara.vim
source ~/.config/nvim/plugins/smooth-scroll.vim
source ~/.config/nvim/plugins/splitjoin.vim
source ~/.config/nvim/plugins/surround.vim
source ~/.config/nvim/plugins/targets.vim
source ~/.config/nvim/plugins/textobj-xmlattr.vim
source ~/.config/nvim/plugins/treesitter.vim
source ~/.config/nvim/plugins/unimpaired.vim
source ~/.config/nvim/plugins/vim-test.vim
source ~/.config/nvim/plugins/vimwiki.vim
source ~/.config/nvim/plugins/visual-multi.vim
source ~/.config/nvim/plugins/visual-star-search.vim
source ~/.config/nvim/plugins/which-key.vim
call plug#end()
doautocmd User PlugLoaded

luafile ~/.config/nvim/lua/lsp/cmake-ls.lua
luafile ~/.config/nvim/lua/plugins/cmp-config.lua
luafile ~/.config/nvim/lua/lsp/cpp-ls.lua
luafile ~/.config/nvim/lua/lsp/csharp-ls.lua
luafile ~/.config/nvim/lua/lsp/css-ls.lua
luafile ~/.config/nvim/lua/lsp/cssmodules-ls.lua
luafile ~/.config/nvim/lua/lsp/deno-ls.lua
luafile ~/.config/nvim/lua/lsp/html-ls.lua
luafile ~/.config/nvim/lua/lsp/json-ls.lua
luafile ~/.config/nvim/lua/lsp/python-ls.lua
luafile ~/.config/nvim/lua/lsp/rust-ls.lua
luafile ~/.config/nvim/lua/lsp/sql-ls.lua
luafile ~/.config/nvim/lua/lsp/svelte-ls.lua
luafile ~/.config/nvim/lua/lsp/typescript-ls.lua
luafile ~/.config/nvim/lua/lsp/vim-ls.lua

"------------------------------------------------------------------------------------------
" Code runners
"------------------------------------------------------------------------------------------
source ~/.config/nvim/runners/csharp.vim
source ~/.config/nvim/runners/python.vim
source ~/.config/nvim/runners/rust.vim

"------------------------------------------------------------------------------------------
" Miscellaneous
"------------------------------------------------------------------------------------------

colorscheme codedark

augroup FileTypeOverrides
    autocmd!
    " Use '//' instead of '/* */' comments
    autocmd FileType cs setlocal commentstring=//%s
    autocmd TermOpen * setlocal nospell
augroup END
