Plug 'tommcdo/vim-lion'

" Remove as many spaces as possible when aligning
let g:lion_squeeze_spaces = 1
