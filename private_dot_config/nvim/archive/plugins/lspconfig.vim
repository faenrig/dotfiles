Plug 'neovim/nvim-lspconfig'

" LSP mappings config
nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> gi <cmd>lua vim.lsp.buf.implementations()<CR>
nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <leader>k <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> <leader>p <cmd>lua vim.lsp.buf.goto_prev()<CR>
nnoremap <silent> <leader>n <cmd>lua vim.lsp.buf.goto_next()<CR>

" autoformat
autocmd BufWritePre *.js lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.jsx lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.cs lua vim.lsp.buf.formatting_sync(nil, 100)
autocmd BufWritePre *.xaml lua vim.lsp.buf.formatting_sync(nil, 100)
