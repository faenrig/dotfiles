# dotfiles

My personal dotfiles.

These can be easily applied with [chezmoi](https://www.chezmoi.io) `chezmoi init --apply <git repository>`
or by cloning this repository and using `chezmoi apply`.
