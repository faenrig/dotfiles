#!/bin/bash
CYAN='\033[0;36m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

printf '
                                ██╗   ██████╗ 
                                ██║   ██╔══██╗
                                ██║   ██████╔╝
                           ██   ██║   ██╔═══╝ 
                           ╚█████╔╝██╗██║██╗  
                            ╚════╝ ╚═╝╚═╝╚═╝  
' | lolcat
printf '
                           Welcome back J.P.!
' | lolcat
echo
# printf $GREEN
# echo
# fortune vimtips
# echo
# printf $NC
